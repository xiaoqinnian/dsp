# 智能业务后台管理系统项目总结

-------
## 项目流程图
![cmd-markdown-logo](http://on-img.com/chart_image/5ad7f54be4b046910641d9ee.png)

## 一、整体技术造型：
> * 搭建vue脚手架
> * js语法 -- ES6
> * css样式 -- LESS
> * 图表 -- ECharts
> * UI -- Element-UI
> * 接口链接 -- Axios


## 技术选型说明
***
> * ###选型ES6语法用意：
> * es6是js的子集，但是我们一般说的js是es6之前的版本。js本身是很不完美的语言，但是es6将js很多丑陋的部分通过语法糖隐藏了。
***
###选型LESS css语法用意：
> *  Less 是一门 CSS 预处理语言，它扩展了 CSS 语言，增加了变量、Mixin、函数等特性，使 CSS 更易维护和扩展。
Less 可以运行在 Node 或浏览器端。
***
###选型首页图表 ECharts用意：
> *  Echarts上手很简单，根据文档里面的提供的实例，选择合适的展示图形，复制左边提供的代码段，拷贝到项目页面加载的js中。
***
###选型Element-UI 用意
> *  Element，一套为开发者、设计师和产品经理准备的基于 Vue 2.0 的桌面端组件库
***
###选型Axios 用意
> *  Axios 是一个基于 promise 的 HTTP 库，可以用在浏览器和 node.js 中。


> ## 路由搭建，路由挂在的实现，权限接口设计

> * 页面一级路由分为两个部分---登录页----Index页面
进入登录页输入根据账号判断权限 判断不同的页面，如果登录成功
后台返回token字段，保存在浏览器localstorge中，设置路由守卫判断
token字段的存在 否则跳转登录页
> * 新建广告
 - 新建广告计划
 - 新建广告单元
 - 新建广告创意 
> * 首页概览
> * 广告管理
 - 广告计划
 - 广告单元
 - 广告创意
> * 数据管理
> * 工具箱
 - 账户管理
 - 客户管理

------

> ## 数据请求
####封装路由
> * 采用axios基于http的数据库运用axios.carete()方法
> * 设置全局请求头 及默认URL路径
> * 添加请求拦截器，用来在发送请求前数据进行操作；
> * 添加响应拦截器，对后台返回数据进行修饰操作；
> * 把操作后的axios挂载在Vue原型上,以便全局使用；
> * 抛出各个接口模块，以便组件中直接使用。


> * 【登录页】：点击登录传输用户名密码，后台判断是否正确 返回对应的字段
判断成功与否且判断权限进入Index页面显示账户信息，浏览器保存加密token字段;
> * 【首页】：进入页面请求首页对应的所有数据并且进行页面的渲染，图表使用Echarts图表生成时想后台请求临时页面的数据，点击日期传送后台对应数据，返回图表对应数据，图表动态渲染
> * 【广告计划】：跳转路由请求相对应的接口渲染页面；
> * 【广告单元】：跳转路由请求相对应的接口渲染页面；
> * 【广告创意】：跳转路由请求相对应的接口渲染页面；
> * 【数据中心】：由于项目时间的关系，先待之前模块之后进行开发；
> * 【 工具箱 】：由于项目时间的关系，先待之前模块之后进行开发；



### 4. 项目中的核心功能

- [x] 首页根据日期时间动态渲染更新ECharts图表
- 手动初始日期结束日期计算格式化日期
- [x] 新建创意页自行封装file运用forData()函数获取file上传数据
- 运用vue插槽slot
- [x] 登录页点击登录触发vuex的action函数异步请求登录接口并在action中接收后台返回参数如果登录成功动态往浏览器中加入token字段

### 5. 数据管理
#### Vuex就是在一个项目中，提供唯一的管理数据源的仓库
> * 用户在组件中的输入操作触发 action 调用；
> * Actions 通过分发 mutations 来修改 store 实例的状态；
> * Store 实例的状态变化反过来又通过 getters 被组件获知。
![cmd-markdown-logo](https://img-blog.csdn.net/20170711164511968?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQveHVzdGFydDc3MjA=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

***
### 6.遇到困到以及解决办法
> * 首先遇到的困难时首页前端进行计算日历开始与结束的遍历循环数据
当时第一想到的是从网上寻找算法，但是并不满足项目自身的要求，无法计算中间值，所以自己封装了一个公共方法，提升了自己的算法；
> * 新建创意页中，点击添加创意按钮生成tab并且"+添加创意"这个按钮并跟随移动,首先想到的是Element-UI有类似此效果，有些出入，为了达到最佳效果，决定自己封装公共的组件以便以后类似效果可以快速实现，模仿框架利用数组控制整个tab的length；点击添加创意按钮实时更新数组长度传入不同的参数用来控制单图与多图的区分，其中用v-if动态重新渲染以实现切换效果，结果每次点击切换后又重新回到初始效果,正因这个特性导致了页面的还原，及时把指令改成v-show利用css属性none、block切换，最终达到最佳效果；



# 整个项目由于时间问题精选  新建广告、首页概览、广告管理页
## 余数据管理、工具箱待前面上线后再进行项目跟进

